//
//  Data.swift
//  HardStyle Podcast
//
//  Created by Aslan Husin on 2018-02-26.
//  Copyright © 2018 Aslan Husin. All rights reserved.
//

import Foundation

final class DataSingleton{
    
    var podcast = [PodcastModel]()
    var episode = [[EpisodeModel]]()
    var currentClickedArtistPodcast: Int?
    var urls = [String]()
    
    static let sharedInstance = DataSingleton()
    private init() {}
    
    
}
