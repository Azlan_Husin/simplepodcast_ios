//
//  PodcastListViewCell.swift
//  HardStyle Podcast
//
//  Created by Aslan Husin on 2018-02-26.
//  Copyright © 2018 Aslan Husin. All rights reserved.
//

import UIKit

class PodcastListViewCell: UITableViewCell {
    
    @IBOutlet weak var podcastArtist: UILabel!
    @IBOutlet weak var podcastPicture: UIImageView!
    @IBOutlet weak var podcastDescription: UILabel!
    @IBOutlet weak var podcastNrOfEpisodes: UILabel!
}
