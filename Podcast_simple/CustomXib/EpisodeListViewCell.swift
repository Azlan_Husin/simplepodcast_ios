//
//  EpisodeListViewCell.swift
//  HardStyle Podcast
//
//  Created by Aslan Husin on 2018-02-27.
//  Copyright © 2018 Aslan Husin. All rights reserved.
//
import UIKit

class EpisodeListViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var pubdate: UILabel!
    @IBOutlet weak var duration: UILabel!
    @IBOutlet weak var downlaod: UIButton!
    
}
