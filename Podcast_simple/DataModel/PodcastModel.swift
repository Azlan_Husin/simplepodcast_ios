//
//  PodcastModel.swift
//  HardStyle Podcast
//
//  Created by Aslan Husin on 2018-02-26.
//  Copyright © 2018 Aslan Husin. All rights reserved.
//

import Foundation
import UIKit

class PodcastModel {
    
    var author: String?
    var sum: String?
    var episode: Int?
    var picUrl: String?
    var coverImage: UIImage?
    
    init(author: String?,sum: String?,episode: Int?,picUrl: String?/*, coverImage: UIImage?*/){
        
        self.author = author
        self.sum = sum
        self.episode = episode
        self.picUrl = picUrl
        //self.coverImage = coverImage
    }
}

