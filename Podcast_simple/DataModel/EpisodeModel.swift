//
//  EpisodeModel.swift
//  HardStyle Podcast
//
//  Created by Aslan Husin on 2018-02-26.
//  Copyright © 2018 Aslan Husin. All rights reserved.
//

import Foundation

class EpisodeModel {
    
    var podcastpos: Int?
    var title: String?
    var link: String?
    var description: String?
    var media: String?
    var duration: String?
    var pubdate: String?
    var getDate: String?
    var downloaded: Int?
    var path: String?
    //var pdate: Date?
    //var DateFormat formatter = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss ZZ", Locale.ENGLISH)
    
    init(podcastpos: Int?, title: String?, link: String?,description : String?, media: String?, duration: String?, downloaded: Int?, path: String?, pubdate: String?){
        
        self.podcastpos = podcastpos
        self.title = title
        self.link = link
        self.description = description
        self.media = media
        self.duration = duration
        self.downloaded = downloaded
        self.path = path
        self.pubdate = pubdate
    }
    
    init(){
        self.podcastpos = 0
        self.title = ""
        self.link = ""
        self.description = ""
        self.media = ""
        self.duration = ""
        self.downloaded = 0
        self.path = ""
    }
}
