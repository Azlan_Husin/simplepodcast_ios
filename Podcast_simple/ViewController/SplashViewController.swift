//
//  SplashViewController.swift
//  HardStyle Podcast
//
//  Created by Aslan Husin on 2018-02-27.
//  Copyright © 2018 Aslan Husin. All rights reserved.
//

import UIKit
import CoreData

class SplashViewController : UIViewController, XMLParserDelegate{
    
    var currentParsingElement:String = ""
    
    var imageUrl:String?
    var titles:String?
    var link:String?
    var descriptions:String?
    var enclosure:String?
    var duration:String?
    var summary:String?
    var pubDate:String?
    var episodes:Int = 0
    var sum:String?
    var author:String?
    var coverimage: UIImage?
    var nrOfPodcast:Int = 0
    let group = DispatchGroup()
    let urlsList = ["https://djisaac.libsyn.com/rss", "https://a2recordsunleashed.libsyn.com/rss", "https://feeds.feedburner.com/keepingtheravealive"]
    var imageUrlOneTime:Int = 0
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    @IBOutlet weak var progressview: UIProgressView!

    override func viewDidLoad() {
        
        super.viewDidLoad()
        progressview.setProgress(0, animated: true)
        downloadList(list: urlsList)        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(false)
        /*let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainNavigationController") as! UINavigationController
        self.present(vc, animated: true, completion: nil)*/
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func downloadList(list: Array<String>){
        for i in 0 ..< list.count {
            
            let url = URL(string: list[i])
            print(list[i])
            
            let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
                if data == nil {
                    print("dataTaskWithRequest error: \(String(describing: error?.localizedDescription))")
                    return
                }
                let parser = XMLParser(data: data!)
                parser.delegate = self
                parser.parse()
            }
            task.resume()
        }
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        currentParsingElement = elementName
        if elementName == "item" {
            //print("Started parsing...")
            episodes += 1
        }
        
        if(self.imageUrlOneTime == 0){
        //get image from for every episode attribute
            if elementName == "itunes:image" {
                let href = attributeDict ["href"] as String?
                self.imageUrl = href
                self.imageUrlOneTime = 1
            }
        }
        
        // get media url
        if elementName == "enclosure" {
            self.enclosure = attributeDict ["url"] as String?
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let foundedChar = string.trimmingCharacters(in:NSCharacterSet.whitespacesAndNewlines)
        
        if (!foundedChar.isEmpty) {
            if currentParsingElement == "title" {
                self.titles = foundedChar
            }
            
            if currentParsingElement == "link" {
                self.link = foundedChar
            }
            
            if currentParsingElement == "description" {
                self.descriptions = foundedChar.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
            }
            
            if currentParsingElement == "itunes:duration" {
                self.duration = foundedChar
            }
            
            if currentParsingElement == "itunes:summary" {
                self.summary = foundedChar
            }
            
            if currentParsingElement == "pubDate" {
                let dateFormatterGet = DateFormatter()
                dateFormatterGet.dateFormat = "EEE, dd MMM yyyy HH:mm:ss ZZ"
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                
                let date: Date? = dateFormatterGet.date(from: foundedChar)

                self.pubDate = dateFormatter.string(from: date!)
            }
            
            if currentParsingElement == "itunes:summary" {
                self.sum = foundedChar
            }
            
            if currentParsingElement == "itunes:author" {
                self.author = foundedChar
            }
            
            /*if currentParsingElement == "url" {
                self.imageUrl = foundedChar
            }*/
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "item" {
            //print("Ended parsing...")
            DataSingleton.sharedInstance.episode.append([EpisodeModel()])
            if(self.descriptions == ">"){
                DataSingleton.sharedInstance.episode[self.nrOfPodcast].insert(EpisodeModel(podcastpos: self.episodes, title: self.titles, link: self.link, description : self.summary,
                                                                                           media: self.enclosure, duration: self.duration, downloaded: 0, path: "", pubdate: self.pubDate), at: episodes - 1)
            }else{
                DataSingleton.sharedInstance.episode[self.nrOfPodcast].insert(EpisodeModel(podcastpos: self.episodes, title: self.titles, link: self.link, description : self.descriptions,
                                                                        media: self.enclosure, duration: self.duration, downloaded: 0, path: "", pubdate: self.pubDate), at: episodes - 1)
            }
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        
        /*playing with coreData
        let context = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "Podcast", in: context)
        let podcast = NSManagedObject(entity: entity!, insertInto: context)
        podcast.setValue(self.author, forKey: "author")
        podcast.setValue(self.sum, forKey: "sum")
        podcast.setValue(self.episodes, forKey: "nrOfEpisodes")
        podcast.setValue(self.imageUrl, forKey: "picUrl")
        
        do{
            try context.save()
        }catch{
            print(error)
        }
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Podcast")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                print(data.value(forKey: "author") as! String)
                print(data.value(forKey: "nrOfEpisodes") as! Int)
            }
            
        } catch {
            print(error)
        }
        */
        
      
        DataSingleton.sharedInstance.podcast.append(PodcastModel(author: self.author,sum: self.sum, episode: self.episodes, picUrl: self.imageUrl/*, coverImage: self.coverimage*/))
        self.episodes = 0
        self.nrOfPodcast += 1
        self.imageUrlOneTime = 0
        if(self.nrOfPodcast == self.urlsList.count){
            DispatchQueue.main.async {
                self.progressview.setProgress(100, animated: true)
                
                if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MainNavigationController") as? UINavigationController
                {
                    self.present(vc, animated: true, completion: nil)
                }
            }
        }
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print("parseErrorOccurred: \(parseError)")
    }
}
