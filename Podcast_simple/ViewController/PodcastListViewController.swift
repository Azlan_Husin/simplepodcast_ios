//
//  ViewController.swift
//  HardStyle Podcast
//
//  Created by Aslan Husin on 2018-02-26.
//  Copyright © 2018 Aslan Husin. All rights reserved.
//

import UIKit

class PodcastListViewController: UITableViewController, XMLParserDelegate, podcastViewDelegate {

    var row = 0
    var task: URLSessionDownloadTask!
    var session: URLSession!
    //to later pass variable and use segue
    let SegueEpisodeListViewController = "SegueEpisodeList"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "PodcastListViewCell", bundle: nil), forCellReuseIdentifier: "idPodcastArtist")
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red:0.00, green:0.00, blue:0.00, alpha:1.0)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.isTranslucent = false
        // Do any additional setup after loading the view, typically from a nib.
        
        session = URLSession.shared
        task = URLSessionDownloadTask()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Dequeue Resuable Cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "idPodcastArtist", for: indexPath)as! PodcastListViewCell
        cell.podcastArtist?.text = DataSingleton.sharedInstance.podcast[indexPath.row].author!
        cell.podcastDescription?.text  = DataSingleton.sharedInstance.podcast[indexPath.row].sum!
        cell.podcastNrOfEpisodes?.text = "Episodes: #\(String(describing: DataSingleton.sharedInstance.podcast[indexPath.row].episode!))"
        cell.podcastPicture?.image = UIImage(named: "placeholder")
        cell.podcastDescription?.sizeToFit()
        
        if(DataSingleton.sharedInstance.podcast[indexPath.row].coverImage == nil){
            let url:URL! = URL(string: DataSingleton.sharedInstance.podcast[indexPath.row].picUrl!)
            task = session.downloadTask(with: url, completionHandler: { (location, response, error) -> Void in
                if let data = try? Data(contentsOf: url){
                    DispatchQueue.main.async(execute: { () -> Void in
                       //check whether the current cell is visible
                        if tableView.cellForRow(at: indexPath) != nil {
                            let img:UIImage! = UIImage(data: data)
                            cell.podcastPicture?.image = img
                            DataSingleton.sharedInstance.podcast[indexPath.row].coverImage = img
                        }
                    })
                }
            })
            task.resume()
        }else{
            cell.podcastPicture?.image = DataSingleton.sharedInstance.podcast[indexPath.row].coverImage
        }
        
        return cell;
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        row = indexPath.row;
        // Perform Segue
        self.performSegue(withIdentifier: SegueEpisodeListViewController, sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == SegueEpisodeListViewController {
            let destinationViewController = segue.destination as? EpisodeListViewController
            destinationViewController?.podcastNr = row
            destinationViewController?.delegate = self
        }
    }
    
    func podcastselected(podcastNr: Int) {
        print(podcastNr)
    }
    
    @IBAction func addRssFeedAction(_ sender: Any) {
        
        let alert = UIAlertController(title: "Add RSS Feed", message: "", preferredStyle: UIAlertController.Style.alert)

        alert.addTextField { (textField: UITextField) in
            textField.placeholder = "https://www.example.com/rss"
        }
        
        let ok = UIAlertAction(title: "OK", style: UIAlertAction.Style.default) { (action: UIAlertAction) in
                                
            if let alertTextField = alert.textFields?.first, alertTextField.text != nil {
                                    
                print("And the URL is \(alertTextField.text!)!")
                                    
            }
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil)
        
        alert.addAction(ok)
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    }
}

