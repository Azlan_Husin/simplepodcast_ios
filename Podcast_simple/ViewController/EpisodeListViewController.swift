//
//  EpisodeListViewController.swift
//  HardStyle Podcast
//
//  Created by Aslan Husin on 2018-02-27.
//  Copyright © 2018 Aslan Husin. All rights reserved.
//

import UIKit

protocol podcastViewDelegate: class {
    func podcastselected(podcastNr: Int)
}

class EpisodeListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var coverimage: UIImageView!
    @IBOutlet weak var desclabel: UILabel!
    @IBOutlet weak var episodeslabel: UILabel!
    weak var delegate: podcastViewDelegate?
    var podcastNr: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "EpisodeListViewCell", bundle: nil), forCellReuseIdentifier: "EpisodeListView")
        if(DataSingleton.sharedInstance.podcast[self.podcastNr!].coverImage != nil){
            coverimage.image = DataSingleton.sharedInstance.podcast[self.podcastNr!].coverImage!
        }
        desclabel.text = DataSingleton.sharedInstance.podcast[self.podcastNr!].sum!
        
        self.navigationItem.title = DataSingleton.sharedInstance.podcast[self.podcastNr!].author!
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataSingleton.sharedInstance.podcast[podcastNr!].episode!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EpisodeListView", for: indexPath)as! EpisodeListViewCell
        cell.desc.text = DataSingleton.sharedInstance.episode[podcastNr!][indexPath.row].description!
        cell.pubdate.text = DataSingleton.sharedInstance.episode[podcastNr!][indexPath.row].pubdate!
        cell.title.text = DataSingleton.sharedInstance.episode[podcastNr!][indexPath.row].title!
        cell.duration.text = DataSingleton.sharedInstance.episode[podcastNr!][indexPath.row].duration!
        
        return cell;
    }
}
